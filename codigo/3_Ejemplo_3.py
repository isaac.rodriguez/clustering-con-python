#CLUSTERING

#Se importan las librerías necesarias
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt

from scipy.cluster.vq import vq

from scipy.cluster.vq import kmeans

#Se crea un array de datos aleatorios, en donde cada fila tiene dos datos (eje X y eje Y)
datos = np.random.random(500).reshape(250, 2)

#Se muestra el array
datos

#Se hace una selección aleatoria para los centroides
centride1 = np.random.choice(range(len(datos)))
centride2 = np.random.choice(range(len(datos)))

#Se genera un solo array para los dos centroides
clusters_centroides = np.vstack([datos[centride1], datos[centride2]])

#Se muestran los centroides
clusters_centroides

#Se hace el clustering con vq donde se le pasan como parámetros los datos y los centroides
clusters = vq(datos, clusters_centroides)
clusters

#Se obtienen las etiquetas de los clusters
etiquetas = clusters[0]
etiquetas

#Se grafican los clusters
colores = np.array(["darkred", "darkblue"])
for i in range(0, len(etiquetas)):
    plt.scatter(x=datos[i,[0]], y=datos[i,[1]], s=25, c=colores[etiquetas[i]])

for i in range(0, len(clusters_centroides)):
    plt.scatter(x=clusters_centroides[i,[0]], y=clusters_centroides[i,[1]], s=200, c=colores[i])
plt.title("K-Means")
plt.xlabel("Eje X")
plt.ylabel("Eje Y")

clusters2 = kmeans(datos, clusters_centroides)
clusters2

clusters3 = kmeans(datos, 2)
clusters3

# Se hace el clustering con el método de k-means donde se le pasa como parámetro 
#el número clusters y con el método fit se le pasa como parámetro los datos
clusters4 = KMeans(n_clusters=2)
clusters4.fit(datos)

#Se obtienen las etiquetas de los clusters
etiquetas2 = clusters4.labels_
etiquetas2

#Se obtienen los centroides de cada cluster
clusters_centroides2 = clusters4.cluster_centers_
clusters_centroides2

#Se grafican los clusters
colores = np.array(["darkred", "darkblue", "darkgreen"])
for i in range(0, len(etiquetas2)):
    plt.scatter(x=datos[i,[0]], y=datos[i,[1]], s=25, c=colores[etiquetas2[i]])

for i in range(0, len(clusters_centroides2)):
    plt.scatter(x=clusters_centroides2[i,[0]], y=clusters_centroides2[i,[1]], s=200, c=colores[i])
plt.title("K-Means")
plt.xlabel("Eje X")
plt.ylabel("Eje Y")

