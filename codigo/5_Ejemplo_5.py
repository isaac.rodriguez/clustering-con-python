#CLUSTERNIG

#Se importan las librerías necesarias
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.cluster import AgglomerativeClustering
from scipy.cluster.hierarchy import dendrogram, linkage

from sklearn.cluster import KMeans
from sklearn import datasets

#Se hace la lectura del dataset y se almacenan en un data frame
data_frame = pd.read_csv("../dataset/wine/winequality-red.csv", sep = ";")

#Se muestran 5 registros del data frame con el método head
data_frame.head()

#Se muestra el tamaño del data frame
data_frame.shape

#Se grafica el histograma de los datos de la columna de 'quality' del  data frame
plt.hist(data_frame["quality"])

#Se hace un grupo con respecto a la columna 'quality' y se calcula la media de cada grupo
data_frame.groupby("quality").mean()

#Se hace la nomalización de los datos
data_frame_norm = (data_frame-data_frame.min())/(data_frame.max()-data_frame.min())

data_frame_norm.head()


#Clustering Jerárquico o Aglomerativo con scikit-learn

#Se crea el cluster Jerárquico
cluster = AgglomerativeClustering(n_clusters=6, linkage="ward").fit(data_frame_norm)
cluster

#Se obtiene  las etiquetas de los clusters
md_h = pd.Series(cluster.labels_)


#Se muestran las etiquetas
md_h

#Se grafica un histograma de los clusters
plt.hist(md_h)
plt.title("Histograma de los clusters")
plt.xlabel("Cluster")
plt.ylabel("Número de vinos del cluster")

#Se muestran los hijos de los clusters
cluster.children_

#Se hace la representación del dendrograma
z = linkage(data_frame_norm, "ward")

#Se grafica el dendrograma
plt.figure(figsize=(25,10))
plt.title("Dendrograma de los vinos")
plt.xlabel("ID del vino")
plt.ylabel("Distancia")
dendrogram(z, leaf_rotation=90, leaf_font_size=8)
plt.show()


#Clustering con K-means

#Se crea el modelo de K-means para el dataset normalizado
modelo = KMeans(n_clusters=6)
modelo.fit(data_frame_norm)

#Se muestran las etiquetas
modelo.labels_

#Se obtiene las etiquetas de los clusters
md_k = pd.Series(modelo.labels_)

#Se agregan las etiquetas de los clusters al dataframe normalizado
data_frame_norm["cluster_h"] = md_h
data_frame_norm["cluster_k"] = md_k

#Se muestran 5 registros del data frame con el método head
data_frame_norm.head()

#Se grafica un histograma de los clusters
plt.hist(md_k)
plt.title("Histograma de los clusters")
plt.xlabel("Cluster")
plt.ylabel("Número de vinos del cluster")


#Se muestran los centroides de los clusters
modelo.cluster_centers_
